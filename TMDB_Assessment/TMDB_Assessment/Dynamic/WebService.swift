//
//  WebService.swift
//  TMDB_Assessment
//
//  Created by Alvin Marana on 23/4/19.
//  Copyright © 2019 Alvin Marana. All rights reserved.
//

import UIKit

class WebService {
    public static let shared: WebService = WebService()
    
    typealias Completion = (Result)->()
    
    private var currentTask: URLSessionDataTask!
    
    private var host = "https://api.themoviedb.org/3"
    private var apiKey = "a9c404dcc57997d39eb05d30de3d5f4a"
    
    private enum API: String {
        case search = "search/movie"
        case featured = "movie/popular"
    }
    
    private func request(api: API, params: [String:String], completion: @escaping Completion) {
        DispatchQueue.global(qos: .background).sync {
            guard var urlComponents = URLComponents(string: "\(host)/\(api.rawValue)") else { return }
            urlComponents.queryItems = self.queryItems(params: params)
            
            guard let url = urlComponents.url else { return }
            
            if self.currentTask != nil {
                self.currentTask.cancel()
            }
            
            self.currentTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                        let parser = Parser()
                        let result = Result()
                        parser.parse(result: result, data: json)
                        
                        DispatchQueue.main.async {
                            completion(result)
                        }
                    } catch {
                        
                    }
                }
            }
            self.currentTask.resume()
        }
    }
    
    public func getImage(withUrlString urlString: String, completion: @escaping (UIImage?)->()) {
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }
        task.resume()
    }
    
    public func suspendRequest() {
        if self.currentTask != nil {
            self.currentTask.cancel()
        }
    }
    
    private func queryItems(params: [String:String]) -> [URLQueryItem] {
        var queryItems: [URLQueryItem] = []
        
        queryItems.append(URLQueryItem(name: "api_key", value: apiKey))
        
        for (key,value) in params {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        
        return queryItems
    }
    
    public func searchMovies(_ query: String, completion: @escaping Completion) {
        let params: [String : String] = [
            "query" : query,
            "language" : "en",
            "page" : "1",
        ]
        self.request(api: .search, params: params, completion: completion)
    }
    
    public func getFeaturedMovies(completion: @escaping Completion) {
        self.request(api: .featured, params: [:], completion: completion)
    }
    
}
