//
//  Movie.swift
//  TMDB_Assessment
//
//  Created by Alvin Marana on 23/4/19.
//  Copyright © 2019 Alvin Marana. All rights reserved.
//

import UIKit

class Movie {
    var title: String!
    var rating: Double!
    var id: String!
    var releaseDate: Date!
    var imagePath: String!
}
