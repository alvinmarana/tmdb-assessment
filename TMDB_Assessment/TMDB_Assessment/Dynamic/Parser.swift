//
//  Parser.swift
//  TMDB_Assessment
//
//  Created by Alvin Marana on 23/4/19.
//  Copyright © 2019 Alvin Marana. All rights reserved.
//

import UIKit

class Parser {
    public func parse(result: Result, data: [String: Any]) {
        var movies: [Movie] = []
        if let results = data["results"] as? [[String: Any]] {
            for movieResult in results {
                movies.append(self.parseMovie(movieResult))
            }
        }
        
        // filter 2017 and 2018 movies
        movies = movies.filter({ (movie) -> Bool in
            let calendar = Calendar.current
            let year = calendar.component(.year, from: movie.releaseDate)
            return year == 2017 || year == 2018
        })
        
        // sort higest rating
        movies = movies.sorted(by: { (movie1, movie2) -> Bool in
            movie1.rating > movie2.rating
        })
        
        result.movies = movies
    }
    
    private func parseMovie(_ data: [String: Any]) -> Movie {
        let movie = Movie()
        movie.id = data["id"] as? String
        movie.title = data["title"] as? String
        movie.rating = data["vote_average"] as? Double
        
        if let releaseDateString = data["release_date"] as? String,
            let releaseDate = Date.date(fromString: releaseDateString) {
            movie.releaseDate = releaseDate
        }
        
        if let imagePath = data["poster_path"] as? String {
            movie.imagePath = "https://image.tmdb.org/t/p/original\(imagePath)"
        }
        return movie
    }
}

class Result {
    public var movies: [Movie]?
}

extension Date {
    public static func date(fromString string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.date(from: string)
    }
    
    public func string() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter.string(from: self)
    }
}

