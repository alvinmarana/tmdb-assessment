//
//  MovieCell.swift
//  TMDB_Assessment
//
//  Created by Alvin Marana on 23/4/19.
//  Copyright © 2019 Alvin Marana. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {
    static let id = "MovieCell"
    
    @IBOutlet weak var label: UITextView!
    @IBOutlet weak var posterImage: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        
        posterImage.clipsToBounds = true
        posterImage.layer.masksToBounds = true
    }
    
    public func configure(movie: Movie) {
        posterImage.image = nil
        dateLabel.text = ""
        ratingLabel.text = ""

        label.text = movie.title
        
        if let releaseDate = movie.releaseDate {
            dateLabel.text = releaseDate.string()
        }
        
        if let rating = movie.rating {
            ratingLabel.text = "\(rating)"
        }
        
        if let imagePath = movie.imagePath {
            WebService.shared.getImage(withUrlString: imagePath) { (image) in
                self.posterImage.image = image
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
