//
//  ViewController.swift
//  TMDB_Assessment
//
//  Created by Alvin Marana on 23/4/19.
//  Copyright © 2019 Alvin Marana. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var featuredMovies: [Movie] = []
    var currentMovieList: [Movie] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: MovieCell.id, bundle: nil), forCellReuseIdentifier: MovieCell.id)
        
        WebService.shared.getFeaturedMovies { (result) in
            guard let movies = result.movies else { return }
            self.featuredMovies = movies
            self.currentMovieList = movies
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMovieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.id) as! MovieCell
        cell.configure(movie: currentMovieList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        
        WebService.shared.suspendRequest()
        
        self.currentMovieList = self.featuredMovies
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            WebService.shared.suspendRequest()

            self.currentMovieList = self.featuredMovies
            self.tableView.reloadData()
        } else {
            WebService.shared.searchMovies(searchText) { (result) in
                guard let movies = result.movies else { return }
                self.currentMovieList = movies
                self.tableView.reloadData()
            }
        }
    }
}

